SHELL=/bin/sh

CC=gcc

ROOTDIR?=""

BMKDIR=$(ROOTDIR)/opt/cern-benchmark
LIBDIR=$(BMKDIR)/lib
RUNDIR=$(BMKDIR)/run
SRCDIR=$(BMKDIR)/src

MAIN=cern-benchmark
SOFTLINK=/usr/bin/cern-benchmark
LIBFILES=lib/*
RUNFILES=run/*
SRCFILES=src/*
WHETSSOURCE=src/whets.c

OTHERFILES=LICENSE README.md Makefile

DIR=$(shell pwd)

all: nocvmfs install_cvmfs

nocvmfs: safeclean prepare install_basedep build_whet

prepare:
	@echo -e "\n -- Prepare installation directories at $(BMKDIR) -- \n"

	@if test ! -d $(BMKDIR); then \
		mkdir -p $(LIBDIR) $(RUNDIR) $(SRCDIR); \
		cp -f $(MAIN) $(OTHERFILES) $(BMKDIR) ; \
		chmod a+x $(BMKDIR)/$(MAIN)	; \
		if test ! -e $(SOFTLINK); then \
			ln -s $(BMKDIR)/$(MAIN) $(SOFTLINK) ;\
		fi ;\
		cp -fr $(LIBFILES) $(LIBDIR) ; cp -f $(RUNFILES) $(RUNDIR) ; cp -f $(SRCFILES) $(SRCDIR) ;\
	else \
		echo "WARN: $(BMKDIR) already exists. do 'make clean' to remove it" ; \
	fi

install_basedep:
	@echo -e "\n -- Install and configure default dependencies from ./lib... -- \n"

	bash -i -c "source $(DIR)/lib/dependencies.sh; base_dependencies; unixbench_dependencies; hs06_dependencies ;"\

install_cvmfs:
	@echo -e "\n -- Install and configure cvmfs ... -- \n"

	bash -i -c "source $(DIR)/lib/dependencies.sh; kv_dependencies; " ;\


build_whet:
	$(CC) -o $(RUNDIR)/whets -DTIME -Wall -pedantic -ansi -s -DNDEBUG -O2 -fomit-frame-pointer -fforce-addr -ffast-math -DDP -DUNIX -DFAST $(WHETSSOURCE) -lm ;

rpminstall:
	@echo -e "\n -- Install dependencies... -- \n"

	wget https://bootstrap.pypa.io/get-pip.py ;\
	python get-pip.py

	export LC_ALL="en_US" ;\
	pip install stomp.py SOAPpy

clean:
	@echo -e "\n -- Deleting $(BMKDIR) and all its content -- \n"

	@if test -d $(BMKDIR); then \
		rm -fr $(BMKDIR) ; \
	fi

	@if test -e $(SOFTLINK); then \
		rm -f $(SOFTLINK) ; \
	fi


safeclean:
	@echo -e "\n -- Backup current $(BMKDIR) and clean it -- \n"

	@if test -d $(BMKDIR); then \
		tar cvzf $(BMKDIR)-$(shell date +"%d%m%y").tar.gz $(BMKDIR) ; make clean ; \
	else \
		if test -e $(SOFTLINK); then \
			rm -f $(SOFTLINK) ; \
		fi ;\
	fi
