# Requirements

The Benchmark Suite has several dependencies and configurations that need to be setup before
actually running the benchmarks. These dependencies vary with the chosen execution mode
and are described on the table below:



| Default mode | Docker mode |
| --- | --- |
| Install: <br>   - wget <br>   - redhat-lsb-core     | Disable IPv6 |
| Download and install _pip_ <sup>1</sup>| Install and run Docker |
| Set *LC_ALL="en_US"* if LC_ALL not set at all <sup>1</sup> | Download Docker custom CERN images, ~ 300 MBs per image<sup> 2</sup> |
| If KV is chosen, _/dev/fuse_ must exist and have 0666 permissions | <small>(_same as default mode_)</small> |
| _pip_ install stomp.py and SOAPpy <sup>1</sup> | |
| Install CVMFS (for KV only) | |
| Make sure _atlas.cern.ch_ CVMFS repository is mounted (for KV only) | |
| Install _gcc_ (for Whetstone) |  <br> |




<sup>1</sup> if **not** in _Offline_ mode

<sup>2</sup> KV has a dedicated Docker image while the other benchmarks and suite
actions run independently on another Docker image.
