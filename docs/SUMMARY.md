# Contents

- [Benchmarking](README.md)
- [How it works](HowItWorks.md)
- [Requirements](Requirements.md)
- [How to run](HowToRun.md)
- [Results](Results.md)