# Results

## Visualizing Data with ElastiSearch

Up to now, all of the produced benchmarking results are stored in [ElastiSearch](https://www.elastic.co/products/elasticsearch)
and visualized in [Kibana](https://www.elastic.co/products/kibana)...

The storing procedures all fully automated and therefore permissions to perform
the same actions shall not be granted to regular users.

_Read_ access on the other hand may be given to anyone using the Benchmark Suite.
This access is managed via the e-group **benchmark-suite-wg**, and any user can
subscribe to it [here](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=benchmark-suite-wg).

_Please note it might take some minutes for your account to get synchronized_

Once this is done, users will be able to visualize data through our
[Kibana interface](https://es-bmkwg.cern.ch).

For more infomation about the monitoring pipeline and how to use Kibana, refer to [CERN IT Monitoring website](http://monit-docs.web.cern.ch/monit-docs/)
## Consuming Raw Data

As explained in the [introduction](README.md), the default transportation layer used for
moving data from the nodes to ES is ActiveMQ. The technology is producer-consumer
based and the current setup also allows users (**with the proper credentials**)
to consume duplicates of the raw data as it is flowing, in real-time and without
any filtering straight from the message brokers.

There are many tools that can be used to consume this data, like the
[STOMP client](https://github.com/cern-mig/stompclt).
