# How to run


## Installation

The Benchmark Suite is delivered as 2 different packages:
 - the _cern-benchmark_ core application, and
 - the _cern-benchmark-docker_ extension to enable the docker mode

To get the application running, one has **3 different options: through _yum_;
directly from the source code; or during runtime**.

#### Install with _yum_

The suite is available in the [ai6.repo](http://linuxsoft.cern.ch/internal/repos/ai6.repo). To install:

```shell
yum install -y cern-benchmark
```
and
```shell
yum install -y cern-benchmark-docker    # optional
```
**Please note** that the latter requires the _cern-benchmark_ package as a dependency.

**Please note (2)** _cern-benchmark_ will require CVMFS as a dependency. The installation
will therefore fail if CVMFS is required and [/etc/yum.repos.d/cernvm.repo](http://cvmrepo.web.cern.ch/cvmrepo/yum/)
(and respective http://cvmrepo.web.cern.ch/cvmrepo/yum/RPM-GPG-KEY-CernVM) is not found.

*It is also **important** to refer that when installing through _yum_, some
application dependencies are not configured but rather installed only, being then
the user's responsibility to provide a proper system setup prior to running the
benchmark suite.

Also, through **yum**, the docker mode will still depend on the core package
since the execution libraries are shared. The dependency decoupling the docker
mode brings is best profited when running directly from the source code.*

---
When installing through _yum_, please make sure to have the following configurations
in place before running the benchmark suite:
 1. CVMFS should have _atlas.cern.ch_ mounted (just for KV benchmark)
 2. if using the _*-docker_ extension:
    - `docker pull ccordeiro/centos-kv`, only for KV benchmark
    - `docker pull ccordeiro/centos-profiler`
    - make sure _docker_ is running

_Please refer to [Requirements](./Requirements.md) for the full list_

#### Install from source code
The source code is available in [GitLab@CERN](https://gitlab.cern.ch/) at
https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite and
https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite-docker.

For the application core do:
```SHELL
git clone https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite.git
cd cloud-benchmark-suite
make all
```
and for the _docker_ mode
```SHELL
git clone https://gitlab.cern.ch/cloud-infrastructure/cloud-benchmark-suite-docker.git
cd cloud-benchmark-suite-docker
make all
```

To delete just the applications, `make clean` will do the job, on both.

#### Install during runtime

One other option is to install and configure all the Benchmark Suite dependencies on-the-fly.
This one is less recommended as it implies that:
 1. the user is calling the application as _root_
 2. as one do not want to loose time checking and/or re-installing/configuring
 dependencies on every run, the user might have a first manual intervention (or hack) to
 specify this installation mode to run just once.


To enable this installation mode, the user is just required to pass the following
argument during the application call: `-i`.

See more about other arguments and on how to call the application below.

## Running benchmarks

Once the suite is installed:

**NOTE:** all actions below also apply to the suite extension _cern-benchmark-docker_
```shell
cern-benchmark -h     # same thing for cern-benchmark-docker
```

```
Usage:
 cern-benchmark [OPTIONS]

OPTIONS:

  -q
 	 Quiet mode. Do not prompt user

  -o
 	 Offline mode. Do not publish results. If not used, the script expects the publishing parameters

  -i
 	 Solves/checks the general and unique dependencies for the specified --benchmarks. If not used, assumes all the dependencies are already installed and configured. NOTE: should run as root

  --benchmarks=<bmk1;bmk2>
 	 (REQUIRED) Semi-colon separated list of benchmarks to run. Available benchmarks are:
     - kv
     - whetstone
     - DB12
     - hyper-benchmark (*)

  --mp_num=#
   Number of concurrent threads (usually cores) to run. If not used, mp_num = cpu_num

  --kv_xml=<xmlFile>
 	 Input file for the KV benchmark. If not provided, SingleMuonGenerator is default

  --uid=<id>
 	 (Optional) Unique identifier for the host running this script. If not specified, it will be generated

  --public_ip=<ip>
 	 (Optional) Public IP address of the host running this script. If not specified, it will be generated

  --cloud=<cloudName>
 	 Cloud name to identify the results - if not specified, CLOUD=test and use -q to avoid prompt

  --vo=<VO>
 	 (Optional) Name of the VO responsible for the underlying resource

  --pnode=<physicalNode>
   (Optional) Name of the hypervisor machine hosting the VM

  --queue_port=<portNumber>
 	 Port number of the ActiveMQ broker where to send the benchmarking results

  --queue_host=<hostname>
 	 Hostname with the ActiveMQ broker where to send the benchmarking results

  --username=<username>
 	 Username to access the ActiveMQ broker where to send the benchmarking results

  --password=<password>
 	 User password to access ActiveMQ broker where to send the benchmarking results

  --amq_key=<path_to_key>
 	 Key file for the AMQ authentication, without passphrase. Expects --amq_cert

  --amq_cert=<path_to_cert>
 	 Certificate for the AMQ authentication. Expects --amq_key

  --topic=<topicName>
 	 Topic (or Queue) name used in the ActiveMQ broker

  --freetext=<string>
   (Optional) Any additional free text to add to the generated output JSON

 (*) this benchmark performs the following measurements sequence:
 1-min Load -> read machine&job features -> DB12 -> 1-min Load -> whetstone -> 1-min Load

 Exiting...

```
**uid** and **public_ip** will be automatically set, during runtime, if not provided!


The desired benchmarks shall be passed as semi-colon separated arguments:
 * kv - for the ATLAS KV. Takes ~15-20 minutes
 * DB12 - for the DIRAC benchmark. Takes order of 2 minutes
 * whetstone - Takes ~2 minutes
 * hyper-benchmark - full sequence takes ~4 minutes.

**Note:** mind the letter case!

### Running without installing

If you have cloned the Git repository and can't perform the *make* or *yum* installation,
you can still run the Benchmark Suite, **providing you have all the dependencies
properly installed and configured already**.

To do so, just instruct the Benchmark Suite to run from the cloned local
repository by doing:
```SHELL
export BMK_ROOTDIR=your_local_dir
```

**NOTE:** when running the benchmark suite without installing it before, the _Whetstone_
benchmark will not be compiled and therefore it is the user's responsibility to do so
and place the executable under _BMK_ROOTDIR/run/whets_ if not there already.


### Getting logs and results in a custom DIR

By default the suite will execute and write into _/tmp_. This behavior can be
altered for those who want to preserve all execution logs and results on
consequent runs.

For this, a environment variable must be set prior to execution:
```SHELL
export BMK_LOGDIR=your_local_dir
```

If the directory does not exist, it will be created automatically.

### Examples

###### running Whetstone quickly, offline
```SHELL
cern-benchmark --benchmarks="whetstone" -q -o   # CLOUD name is set to test by default
```
same thing with docker:
```SHELL
cern-benchmark-docker --benchmarks="whetstone" -q -o
```
###### running KV, DIRAC Benchmark and Whetstone
offline:
```SHELL
cern-benchmark --benchmarks="kv;DB12;whetstone" --cloud=CERN -o
```
online:
```SHELL
cern-benchmark --benchmarks="kv;DB12;whetstone" --cloud=CERN --queue_host=AMQ_MB_SERVER.domain
--queue_port=PORT_NUMBER --username=yourUser --password=`cat /pwdfile` --topic=topicORqueueName
```

###### running KV, in just one core
offline:
```SHELL
cern-benchmark --benchmarks="kv" --cloud=CERN -o --mp_num=1
```

###### specifying the host metadata
```SHELL
cern-benchmark --benchmarks="KV" --vo=ATLAS
--public_ip=`curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'`
--uid=`hostname`_$(date -d  "`who -b | sed -e 's@system boot @@'`" +%s) --cloud=CERN
--queue_host=AMQ_MB_SERVER.domain --queue_port=PORT_NUMBER --username=yourUser
--password=`cat /pwdfile` --topic=topicORqueueName
```

###### publishing to AMQ using certificate authentication
**NOTE 1**: for certificate authentication, your DN needs to be added on the AMQ side.
Please make a request for it.

**NOTE 2**: please notice the port numbers for plain and certificate authentication
are different

```SHELL
cern-benchmark --benchmarks="whetstone" -q --cloud=test --vo=test --freetext="test"
--queue_host=AMQ_MB_SERVER.domain --queue_port=PORT_NUMBER_SSL --username=yourUser
--amq_key=/yourkey.key --amq_cert=/yourcert.crt --topic=topicORqueueName
```


__NOTE__: _DB12 = fastBmk_ for older versions
