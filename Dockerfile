FROM gitlab-registry.cern.ch/cloud-infrastructure/cloud-benchmark-suite/hepworkloads-base:1.0

COPY . /to_be_installed

LABEL multi.maintainer="Domenico Giordano <domenico.giordano@cern.ch>" \
      multi.version="0.1" \
      multi.description="container of the full suite"

RUN yum install -y yum-plugin-ovl #because of https://github.com/CentOS/sig-cloud-instance-images/issues/15
RUN cd /to_be_installed ; make nocvmfs; rm -rf /to_be_installed

