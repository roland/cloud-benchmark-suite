#!/bin/bash

export DEBUG=0
export dateTag

function doOne(){
    [ $DEBUG -gt 0 ] && echo "`date` : start process $i"; 
 
    WDIR=$BASE_WDIR/proc_$1
    [ $DEBUG -gt 0 ] &&echo $WDIR
    [ -e $WDIR ] && rm -f $DIR
    mkdir -p $WDIR
    cp preInclude.SingleMuonGenerator.py $WDIR
    cp -r sqlite200 $WDIR
    cd $WDIR

    sleep 5

    /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0/AtlasProduction/17.8.0.9/InstallArea/share/bin/AtlasG4_trf.py outputHitsFile='KitValidation-SimulHITS-17.8.0.9.pool.root' maxEvents='100' skipEvents='0' preInclude='KitValidation/kv_reflex.py,preInclude.SingleMuonGenerator.py' geometryVersion='ATLAS-GEO-16-00-00' conditionsTag='OFLCOND-SDR-BS7T-04-03' 2>&1 >out_$1.log

    [ $DEBUG -gt 0 ] && echo -e "\n`date` : done process $i\n"
}


function parseResults(){
    echo "Parsing results from " $BASE_WDIR/proc_*/out_*.log
    #Parsing  Event Throughput: xxxx ev/s

    res=`grep -A1 "INFO Statistics for 'evt'" $BASE_WDIR/proc_*/AtlasG4_trf.log | grep "<cpu>" | sed -e "s@[^(]*([[:blank:]]*\([ 0-9\.]*\) +/-.*@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;values="";}
{ val=1./(int($1)/1000.); a[count]=val; values=sprintf("%.6f,%s",val,values); count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val}
END{
    n = asort(a); if (n % 2) {
       median=a[(n + 1) / 2];
    } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
    };
    gsub(/,$/,"",values);
    printf "\"cpu_score\": %.4f, \"cpu_score_values\": [%s], \"cpu_score_stat\" :{\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f}", sum, values, sum/count, median, amin, amax 
}' || echo "\"err\": \"[ERROR] Something went wrong in parsing the CPU score\""`

    echo -e "{\"copies\":$ncopies , \"threads_x_copy\":1 , \"events_x_thread\" : 100 , $res , \"app\": \"KV_17.8.0.9_SingleMuon\"}" > $BASE_WDIR/kv_summary.json
    cat $BASE_WDIR/kv_summary.json
}


export dateTag=`date +%s`
export TESTID=kv_${dateTag}_$RANDOM
export BASE_WDIR=/results/${TESTID}
export ncopies=`nproc`
while getopts "dhn:w:" o; do
  case ${o} in
    n)
      [ $OPTARG -gt 0 ] && ncopies=$OPTARG
      ;;
    w)
      BASE_WDIR=$OPTARG
      ;;
    d)
      DEBUG=1
      ;;
    h)
      echo "usage: $0 -n <num_copies [`nproc`]> [-d (for debug verbosity)]"
      exit 0
      ;;
  esac
done

echo "ncopies= $ncopies"

BMKDIR=`dirname $0`
cd $BMKDIR

echo "WORKING DIR=$BASE_WDIR"

##extracted from source /code/resources/env.sh 17.8.0.9,AtlasProduction
export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
source $AtlasSetup/scripts/asetup.sh 17.8.0.9,AtlasProduction


for i in `seq 1 $ncopies`;
do
(   doOne $i; )&
done

FAIL=0
for job in `jobs -p`
do
[ $DEBUG -gt 0 ] && echo "...waiting forked process " $job
    wait $job || let "FAIL+=1"
done
[ $DEBUG -gt 0 ] && echo "FAILS " $FAIL

#dateTag=1493069996
parseResults
#tar -cvjf $BASE_RDIR/${TESTID}.tar.bz2 $BASE_WDIR

