#------------------------------------------------------------------------------
# SPEC CPU2017 config file for: gcc / g++ / gfortran on Linux x86
#------------------------------------------------------------------------------
# 
# Usage: (1) Copy this to a new name
#             cd $SPEC/config
#             cp Example-x.cfg myname.cfg
#        (2) Change items that are marked 'EDIT' (search for it)
# 
# SPEC tested this config file with:
#    Compiler version(s):    4.4.7, 4.9.2, 5.2.0, 6.3.0
#    Operating system(s):    Oracle Linux Server 6.5 and 7.2 / 
#                            Red Hat Enterprise Linux Server 6.5 and 7.2
#    Hardware:               Xeon 
#
# If your system differs, this config file might not work.
# You might find a better config file at http://www.spec.org/cpu2017/results
#
# Known Limitations with GCC 4 
#
#   (1) Possible problem: compile time messages
#                             error: unrecognized command line option '...'
#      Recommendation:    Use a newer version of the compiler.
#                         If that is not possible, remove the unrecognized 
#                         option from this config file.
#
#   (2) Possible problem: run time errors messages 
#           527.cam4_r or 627.cam4_s    *** Miscompare of cam4_validate.txt    
#      Recommendation: Use a newer version of the compiler.
#                      If that is not possible, try reducing the optimization.
#
# Compiler issues: Contact your compiler vendor, not SPEC.
# For SPEC help:   http://www.spec.org/cpu2017/Docs/techsupport.html
#------------------------------------------------------------------------------


#--------- Label --------------------------------------------------------------
# Arbitrary string to tag binaries 
#                  Two Suggestions: # (1) EDIT this label as you try new ideas.
%define label mytest                # (2)      Use a label meaningful to *you*.


#--------- Preprocessor -------------------------------------------------------
%ifndef %{bits}                # EDIT to control 32 or 64 bit compilation.  Or, 
%   define  bits        64     #      you can set it on the command line using:
%endif                         #      'runcpu --define bits=nn'

%ifndef %{build_ncpus}         # EDIT to adjust number of simultaneous compiles.
%   define  build_ncpus 8      #      Or, you can set it on the command line: 
%endif                         #      'runcpu --define build_ncpus=nn'

# Don't change this part.
%define    os           LINUX
%if %{bits} == 64
%   define model        -m64
%elif %{bits} == 32
%   define model        -m32
%else
%   error Please define number of bits - see instructions in config file
%endif


#--------- Global Settings ----------------------------------------------------
# For info, see:
#            https://www.spec.org/cpu2017/Docs/config.html#fieldname   
#   Example: https://www.spec.org/cpu2017/Docs/config.html#tune        

command_add_redirect = 1
flagsurl             = $[top]/config/flags/gcc.xml
ignore_errors        = 1
iterations           = 1
label                = %{label}-m%{bits}
line_width           = 1020
log_line_width       = 1020
makeflags            = --jobs=%{build_ncpus} 
mean_anyway          = 1
output_format        = txt,html,cfg,pdf,csv
preenv               = 1
reportable           = 0
tune                 = base


#--------- How Many CPUs? -----------------------------------------------------
# Both SPECrate and SPECspeed can test multiple chips / cores / hw threads
#    - For SPECrate,  you set the number of copies.
#    - For SPECspeed, you set the number of threads. 
# See: https://www.spec.org/cpu2017/Docs/system-requirements.html#MultipleCPUs
#
#    q. How many should I set?  
#    a. Unknown, you will have to try it and see!
#
# To get you started, some suggestions:
#
#     copies - This config file defaults to testing only 1 copy.   You might
#              try changing it to match the number of cores on your system, 
#              or perhaps the number of virtual CPUs as reported by:
#                     grep -c processor /proc/cpuinfo
#              Be sure you have enough memory.  See:
#              https://www.spec.org/cpu2017/Docs/system-requirements.html#memory
#
#     threads - This config file sets a starting point.  You could try raising 
#               it.  A higher thread count is much more likely to be useful for
#               fpspeed than for intspeed.
#
intrate,fprate:
   copies           = 1   # EDIT to change number of copies (see above)
intspeed,fpspeed:
   threads          = 4   # EDIT to change number of OpenMP threads (see above)


#------- Compilers ------------------------------------------------------------
default:
#                    EDIT: the directory where your compiler is installed
%ifndef %{gcc_dir}
%   define  gcc_dir      /usr # /opt/rh/devtoolset-2/root/usr/  #/SW/compilers/GCC/Linux/x86_64/gcc-6.3.0
%endif

# EDIT if needed: the preENV line adds library directories to the runtime
#      path.  You can adjust it, or add lines for other environment variables. 
#      See: https://www.spec.org/cpu2017/Docs/config.html#preenv
#      and: https://gcc.gnu.org/onlinedocs/gcc/Environment-Variables.html
   preENV_LD_LIBRARY_PATH  = %{gcc_dir}/lib64/:%{gcc_dir}/lib/:/lib64
  #preENV_LD_LIBRARY_PATH  = %{gcc_dir}/lib64/:%{gcc_dir}/lib/:/lib64:%{ENV_LD_LIBRARY_PATH}
   SPECLANG                = %{gcc_dir}/bin/
   CC                      = $(SPECLANG)gcc     -std=c99   %{model}
   CXX                     = $(SPECLANG)g++                %{model}
   FC                      = $(SPECLANG)gfortran           %{model}
   # How to say "Show me your version, please"
   CC_VERSION_OPTION       = -v
   CXX_VERSION_OPTION      = -v
   FC_VERSION_OPTION       = -v

default:
%if %{bits} == 64
   sw_base_ptrsize = 64-bit
   sw_peak_ptrsize = 64-bit
%else
   sw_base_ptrsize = 32-bit
   sw_peak_ptrsize = 32-bit
%endif


#--------- Portability --------------------------------------------------------
default:               # data model applies to all benchmarks
%if %{bits} == 32
    # Strongly recommended because at run-time, operations using modern file 
    # systems may fail spectacularly and frequently (or, worse, quietly and 
    # randomly) if a program does not accommodate 64-bit metadata.
    EXTRA_PORTABILITY = -D_FILE_OFFSET_BITS=64
%else
    EXTRA_PORTABILITY = -DSPEC_LP64
%endif

# Benchmark-specific portability (ordered by last 2 digits of bmark number)

500.perlbench_r,600.perlbench_s:  #lang='C'
%if %{bits} == 32
%   define suffix IA32
%else
%   define suffix X64
%endif
   PORTABILITY    = -DSPEC_%{os}_%{suffix}

521.wrf_r,621.wrf_s:  #lang='F,C'
   CPORTABILITY  = -DSPEC_CASE_FLAG 
   FPORTABILITY  = -fconvert=big-endian

523.xalancbmk_r,623.xalancbmk_s:  #lang='CXX'
   PORTABILITY   = -DSPEC_%{os}

526.blender_r:  #lang='CXX,C'
   PORTABILITY   = -funsigned-char -DSPEC_LINUX

527.cam4_r,627.cam4_s:  #lang='F,C'
   PORTABILITY   = -DSPEC_CASE_FLAG

628.pop2_s:  #lang='F,C'
   CPORTABILITY    = -DSPEC_CASE_FLAG
   FPORTABILITY    = -fconvert=big-endian


#-------- Tuning Flags common to Base and Peak --------------------------------

#
# Speed (OpenMP and Autopar allowed)
#
%if %{bits} == 32
   intspeed,fpspeed:
   #
   # Many of the speed benchmarks (6nn.benchmark_s) do not fit in 32 bits
   # If you wish to run SPECint2017_speed or SPECfp2017_speed, please use
   #
   #     runcpu --define bits=64
   #
   fail_build = 1
%else
   intspeed,fpspeed:
      EXTRA_OPTIMIZE = -fopenmp -DSPEC_OPENMP
   fpspeed:
      #
      # 627.cam4 needs a big stack; the preENV will apply it to all 
      # benchmarks in the set, as required by the rules.  
      #
      preENV_OMP_STACKSIZE = 120M
%endif


#--------  Baseline Tuning Flags ----------------------------------------------
#
# EDIT if needed -- Older GCC might not support some of the optimization
#                   switches here.  See also 'About the -fno switches' below.
#
default=base:         # flags for all base 
   OPTIMIZE       = -g -O3 -fPIC -pthread #-march=native -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize

intrate,intspeed=base: # flags for integer base
%if %{bits} == 32                    
    EXTRA_COPTIMIZE = -fgnu89-inline -fno-strict-aliasing 
%else                                
    EXTRA_COPTIMIZE = -fno-strict-aliasing 
    LDCFLAGS        = -z muldefs     
%endif                               
# Notes about the above
#  - 500.perlbench_r/600.perlbench_s needs -fno-strict-aliasing.
#  - 502.gcc_r/602.gcc_s             needs -fgnu89-inline or -z muldefs
#  - For 'base', all benchmarks in a set must use the same options.   
#  - Therefore, all base benchmarks get the above.  See:
#       www.spec.org/cpu2017/Docs/runrules.html#BaseFlags  
#       www.spec.org/cpu2017/Docs/benchmarks/500.perlbench_r.html
#       www.spec.org/cpu2017/Docs/benchmarks/502.gcc_r.html


#--------  Peak Tuning Flags ----------------------------------------------
default=peak:
   basepeak = yes  # if you develop some peak tuning, remove this line.
   #
   # -----------------------
   # About the -fno switches
   # -----------------------
   #
   # For 'base', this config file (conservatively) disables some optimizations.
   # You might want to try turning some of them back on, by creating a 'peak' 
   # section here, with individualized benchmark options:
   #
   #        500.perlbench_r=peak:
   #           OPTIMIZE = this
   #        502.gcc_r=peak:
   #           OPTIMIZE = that 
   #        503.bwaves_r=peak:
   #           OPTIMIZE = other   .....(and so forth)
   #
   # If you try it:
   #   - You must remove the 'basepeak' option, above.
   #   - You will need time and patience, to diagnose and avoid any errors.
   #   - perlbench is unlikely to work with strict aliasing 
   #   - Some floating point benchmarks may get wrong answers, depending on:
   #         the particular chip 
   #         the version of GCC 
   #         other optimizations enabled
   #         -m32 vs. -m64
   #   - See: http://www.spec.org/cpu2017/Docs/config.html
   #   - and: http://www.spec.org/cpu2017/Docs/runrules.html


#------------------------------------------------------------------------------
# Tester and System Descriptions - EDIT all sections below this point              
#------------------------------------------------------------------------------
#   For info about any field, see
#             https://www.spec.org/cpu2017/Docs/config.html#fieldname 
#   Example:  https://www.spec.org/cpu2017/Docs/config.html#hw_memory  
#-------------------------------------------------------------------------------

#--------- EDIT to match your version -----------------------------------------
default:
   sw_compiler001   = C/C++/Fortran: Version 6.2.0 of GCC, the
   sw_compiler002   = GNU Compiler Collection

#--------- EDIT info about you ------------------------------------------------
# To understand the difference between hw_vendor/sponsor/tester, see:
#     https://www.spec.org/cpu2017/Docs/config.html#test_sponsor
intrate,intspeed,fprate,fpspeed: # Important: keep this line
   hw_vendor          = My Corporation
   tester             = My Corporation
   test_sponsor       = My Corporation
   license_num        = nnn (Your SPEC license number) 
#  prepared_by        = # Ima Pseudonym                       # Whatever you like: is never output


#--------- EDIT system availability dates -------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
   hw_avail           = # Nov-2099                            # Date of LAST hardware component to ship
   sw_avail           = # Nov-2099                            # Date of LAST software component to ship

#--------- EDIT system information --------------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
#  hw_cpu_name        = # Intel Xeon E9-9999 v9               # chip name
   hw_cpu_nominal_mhz = # 9999                                # Nominal chip frequency, in MHz
   hw_cpu_max_mhz     = # 9999                                # Max chip frequency, in MHz
#  hw_disk            = # 9 x 9 TB SATA III 9999 RPM          # Size, type, other perf-relevant info
   hw_model           = # TurboBlaster 3000                   # system model name
#  hw_nchips          = # 99                                  # number chips enabled
   hw_ncores          = # 9999                                # number cores enabled
   hw_ncpuorder       = # 1-9 chips                           # Ordering options
   hw_nthreadspercore = # 9                                   # number threads enabled per core
   hw_other           = # TurboNUMA Router 10 Gb              # Other perf-relevant hw, or "None"

#  hw_memory001       = # 999 GB (99 x 9 GB 2Rx4 PC4-2133P-R, # The 'PCn-etc' is from the JEDEC 
#  hw_memory002       = # running at 1600 MHz)                # label on the DIMM.

   hw_pcache          = # 99 KB I + 99 KB D on chip per core  # Primary cache size, type, location
   hw_scache          = # 99 KB I+D on chip per 9 cores       # Second cache or "None"
   hw_tcache          = # 9 MB I+D on chip per chip           # Third  cache or "None"
   hw_ocache          = # 9 GB I+D off chip per system board  # Other cache or "None"

   fw_bios            = # American Megatrends 39030100 02/29/2016 # Firmware information
#  sw_file            = # ext99                               # File system
#  sw_os001           = # Linux Sailboat                      # Operating system
#  sw_os002           = # Distribution 7.2 SP1                # and version
   sw_other           = # TurboHeap Library V8.1              # Other perf-relevant sw, or "None"
#  sw_state           = # Run level 99                        # Software state.

# Note: Some commented-out fields above are automatically set to preliminary 
# values by sysinfo
#       https://www.spec.org/cpu2017/Docs/config.html#sysinfo
# Uncomment lines for which you already know a better answer than sysinfo 

__HASH__
508.namd_r=base=mytest-m64:
# Last updated 2018-03-16 09:26:22
opthash=3c9f7ae9eea87e94cd98a8ed5df95787ebb49bf6243bebe79b92eb3c02b0c98f
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNqVUFtLwzAUfs+vOOR1ZJ04BMs66CXOatuEpoXqS3C1G1XXSC+C/96ss9OK4MxLPjjfOd8lUhXZ\
PTwXm/KlAPXalqpqTNS0dZm3su6qx7KWb0Vdbt4tfIaRho2mWHg2vZzNMUIuC7kJ2Oia2liXlbGd\
TODHI7uLOZAciBoEpgqIJzh19Rd51ElXsEd26EnPF7YTUCkE/eRIO02YFCnnMRVCMk6jkAPZAmHn\
XxoHasC1FCwa1dV5scTINcHNMutPewOTOTeMJxYeucVIJ3R5ehXYK6FnY+f91KGRey31gSPnP2Ew\
YiZoXT/076le7qP1d2mWxLbkLE5sxw/85G6Q75NiFPjR7SntH9C4Mlio9VORt81yRD/GBuhbCbxT\
6vs1QL/N0n2f38r8AKG1oc8=
exehash=89e696f6877e5b6ff7c57cfa6c7a0539e69986a93ccdb3cadd09c84b1ae88e8f

510.parest_r=base=mytest-m64:
# Last updated 2018-03-16 09:28:44
opthash=f8139becda9f7ca23fd12695443d8a02451179a8971a4de77e8021f5419e8ca7
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNqNUF1LwzAUfc+vuOR1pJs4BMs66Eec1bYJ/YDpS3BdN6JbI/0Q/PdmnVUrgs1LDtxzz7nnRKok\
x6eXYicPBajXRqqyNlHdVDJvRNWWW1mJt6KSu3cLX2CkYa0pFp4Z17M5RshlITcBT9u6mm5kOd1P\
JvDrkePVHEgORPUGhgLiJZy6+os86mQrIL4s80O7LTQyPqfCzlImkozzmCaJYJxGIdd6eyDs8lv+\
zA24doFFrdoqL5YYuSa467X172U9kzl3jKcWHhyKkQ7n8uwmsFeJng2P7qYOjdxb0RPGpsCImaD9\
/NB/pHqvi9Tp0XUa24KzOLUdP/DTh962S4hR4Ef3Ywo/o2FVsFCb5yJv6uWA/hUXoGsj8MbU9meA\
bptlpx5/lPgBzCWevA==
exehash=359cfa095475b96665a6d4d1be4cc0f562c5cbbcdd13d7092f9144050b22ee11

511.povray_r=base=mytest-m64:
# Last updated 2018-03-16 09:28:55
opthash=8581c381402688f9122c21c7f61d5b0139385c8e613e865ece0126e37fb0e16f
baggage=
compiler_version=\
@eNrtk01zmzAQhu/+FTomEwv8VdfjGU5Mpu2l6aROJzdGiDUsERLVh03z6yvBEJK0l9x9ALTPu1ot\
u1qSPj6m2a/b+5/f7r5ndz8O/rOfkfRf9mBQliR3KCxFSUwL3ESzA9Ml2D3pdttsu6EaiopZKlC6\
bpYqecTSeUbOaKs9iaKYj4xQ2mo4YpfEzmhvNUwWqHsrNhXTEHviOcqjeicE5JUQlObORxNJZW27\
j2NvPaMQLBoSibhqXpjfAZLlAmiulDVWs3ZCfeBism2lgRUmaZXBbsK8Av7k65BoEMAMjFmYP8ZC\
Q58F5pNzlvGOZcxCh9bTAk2PvY+TZ5QFhY5Da1FJM20qpaNO4m8HVOU1cDtJgsnSsRJMwuf85mbu\
dR5eNBg1O7H5UWn/W3LOild/GxTKzjYp7dOrNApjcEy/d6lUA0OVfYZxfWriHi+jT9GClrweVtEi\
rvvujUlhHrTGCYtHFPDu3IahtP4BTRtVvNQL/JaavWlqcI+BC2wNBD3y+tuqDQHDQUOZ+0htK8Yl\
F0qVo2GdhKQEfzDykTHNq2y9SnC723oW7nKR/O/mHvruk5Cx2JP+DsxKzskJtPHtIptoE30mq8Vy\
tVgv1+Tq3l+dr8wOnC531+TqS5pek8vQXIbmMjQfHZq/I55BKw==
compile_options=\
@eNrtU9FqwjAUfc9XhLxK1TEZWKzQ1sx1q02wLbi9BI1Rus3GJXWwvzdr1226gcIexpiBkgs56T3n\
3JxI5tZq+iAW2aOAcl1kMtc20IXKeMHUJp9nij0LlS1eHHSGgCm1gTio3ey2OwgAn4yoDVFro1Vr\
luWtZaMB95a1uuhAi0NL1g2aElqDmGLfbNEAe+kQWsFCybwQ+dyUs6kWZmuaT68FZ09aquLtCnPT\
hLA4pXSM45gRiqMRNU2W0CLnHz0rbEhNa9jTcqO46CPg29CfTJyDdGsk8a4JTRy0wx4Bo9in6WXo\
DmNztqukPPVw5F+xGvAjaQgQGxoSwSi4w+Znpc6yCZ4kY5dRMk5cLwiD5LbmUspGX0fDeSVQF3OH\
d7u/NJqDA/Kdg5wr4J+eThhEN8cEp6r2LOzJ2b3ghe7vwN89gLA0KBwc89K/FVDeJumruZ+cPaX9\
lPZT2v9L2rdUTyqt
exehash=744113dd2c8febbc25d9c21abb5db65253e53dcfab47f23eda8b66a9637a812f

526.blender_r=base=mytest-m64:
# Last updated 2018-03-16 09:30:53
opthash=e11dd88e2e295f9fdf0694454b641bfc468283035cec96e0efd4a95c1365d054
baggage=
compiler_version=\
@eNrtk01zmzAQhu/+FTomEwv8VdfjGU5Mpu2l6aROJzdGiDUsERLVh03z6yvBEJK0l9x9ALTPu1ot\
u1qSPj6m2a/b+5/f7r5ndz8O/rOfkfRf9mBQliR3KCxFSUwL3ESzA9Ml2D3pdttsu6EaiopZKlC6\
bpYqecTSeUbOaKs9iaKYj4xQ2mo4YpfEzmhvNUwWqHsrNhXTEHviOcqjeicE5JUQlObORxNJZW27\
j2NvPaMQLBoSibhqXpjfAZLlAmiulDVWs3ZCfeBism2lgRUmaZXBbsK8Av7k65BoEMAMjFmYP8ZC\
Q58F5pNzlvGOZcxCh9bTAk2PvY+TZ5QFhY5Da1FJM20qpaNO4m8HVOU1cDtJgsnSsRJMwuf85mbu\
dR5eNBg1O7H5UWn/W3LOild/GxTKzjYp7dOrNApjcEy/d6lUA0OVfYZxfWriHi+jT9GClrweVtEi\
rvvujUlhHrTGCYtHFPDu3IahtP4BTRtVvNQL/JaavWlqcI+BC2wNBD3y+tuqDQHDQUOZ+0htK8Yl\
F0qVo2GdhKQEfzDykTHNq2y9SnC723oW7nKR/O/mHvruk5Cx2JP+DsxKzskJtPHtIptoE30mq8Vy\
tVgv1+Tq3l+dr8wOnC531+TqS5pek8vQXIbmMjQfHZq/I55BKw==
compile_options=\
@eNrtnF1zozYUhu/9Kzy+3fGSbdJtk1nvjD/Y2K1jM/7oZnvDyCBj1YBYSdhJf30FjpM4GFA77Uwv\
3twYdF4OQkjinGcUTXjcjsiWrllImzxRjMfypiGVYJ5yRRr7TLg7Ktj6sdP60GroQ6klndbF++uL\
q1aj0Z/eOTfNlpVKYa1YbAXv3jXf/LWjj1fNttds8+MN3vNmezB37L7+mQzs3vK22R7JhHqCxL4+\
XIU09qmw6IOiIs78Chpyz2KxF6Y+LSo8HpLIt0Zl9iCk+/KrQ7aKdiXF+qrspGjVVQ2oEvSMv723\
Z77avDIUnBwKLKJ4xLwz5alPZEI8Wnql9+iFVFpPpUU7DVeURk81KpqDDZeqpLj0nlkrtqOHUkP5\
hSkRPvVJqF9itbXUBdtKHu6e27go0BeTkBbLIxqlioVnDGy7VXkjF208obHuU1wwXjQKIrfqXJvL\
iG9paQWzMRUHxfJUrT0ev+5/kqdCv/rjafa75rGqVxRb74xwqxU0NNEYudOjpFZg5oiT055cqql3\
F1G5qTGbObEU56EsF+lOEhKfVAmihEumeMWTUT+zS4vELCLZBGkgFVqZClqv9FKxM5AFWY9nYb2w\
OIuWCnX7rk+HV5mU12siPeoMVJUv/kWlyErPN/VKvvqDeqpel2weJfNkvVC8na9LdNLTn5bYRJeG\
iXITwmKDaurikw9smSybFV3imfXFg3qVKqU/7aZyL2SJsVb75SE1leehjKE2ECTZmIr16AyMPTM9\
I5tqQx6cRAGV4jgkxlLuG9eXpypksVHXzPW6g7JEGavp91RPMObulf6MGmtZZPyYqaQiEXRtqt8x\
ur80GDJKx4NyzUVUL30TjpSpdtlRuS5I0kpj7feNbZMwDZiBot5VtErXNWYzJ68DMPpQ0VuynEX6\
MalRCBNFbdUi7rM106mPgaTWWTYoZY35OWygdUKju1lyUx1bHVTZmKuMKZJHtan6IoiTNKg2UhCn\
Ef7fkwvyqMdcVXSxZ7HP9xGJ9ax95tkDEukvrO7e1OodLDM9B+rTVy/HzVLa2l7zUnMd6yZ5jP9n\
HhbnOa7bXS6m7nzpODN7Pnenjj25c05svW8Lezob2LPOxcOHHy6vfvz4089acDu2v7qTqWvPjyfz\
RXcxypLmr6PF0B1Muu7tsDsfHgtux64zm34ZjW03S8u7i6NhaHcHY31zfT7s/ma7y8l8dDuxB25/\
2J3pBF3XeHqZp+rrNJYsiKnf9jZEHGs5Hk2W94dc/qnA0Sl989OhZT63Gv2bZv/+vlOLAY7Kae+X\
qbPotE6oQKvhaKOz/DLu3s617ZQQ5NaePekP3aMAyADIAMgAyADIAMgAyADIAMgAyADIAMgAyADI\
AMjgP0QGrcb0pqkT+NHd6HdbJ+I5P8gTdGc6W3R7o/Fo8U2XV9CEXG3fL2Zd9/SaF8DQKi4u8LwD\
SpDK73jX11hcAFIAUgBSAFIAUgBSAFIAUgBSAFIAUgBSAFIAUvB/W1xgtMCgcolBv1PLAg5CrC8A\
NQA1ADUANQA1ADUANQA1ADUANQA1ADUANQA1wPqCp/UFWvmryeYFh6MXkFFLMT4dgjT5+cTVM4lo\
NnNMMR6Y/MfEv9AU+b2mywyIvKIh2LsBoASgBKAEoASgBKAEoASgBKAEoASgBKAEoASgBHs3YO8G\
IAMgAyADIAMgAyADIAMgAyADIAMgAyADIAMgA6ytwN4NIAUgBSAFIAUgBSAFIAUgBSAFIAUgBSAF\
IAUgBdi7AXs3gBqAGoAagBqAGoAagBqAGoAagBqAGoAagBqAGmB9AfZu+Od7N/wFYDtExw==
exehash=737ee986f2ba3e5ba90b3d239f8fc5cc85d876da962630fad7b651ff1b345f8d

520.omnetpp_r=base=mytest-m64:
# Last updated 2018-03-16 09:31:19
opthash=0d66c67f7d97d8ebb4ba21bb312edb14f78e7a0dee98451044ac833594a70d54
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNq1UVtPwjAUfu+vaPpKxjBeEhdGskuF6libXSL60sAYpMpWsm4m/HvrcMqMibzYl3PS851+l4ay\
NIrla74RuxzKfS1kqSyg6kpkNa+aci0q/pZXYnOw0QUCulUaYqPR8HZ0jQDw6JxZEJmNqsyVKM3t\
YAB/HKO4uYJGBg3ZEQwlNPyYYU+X0MduOoUGUaJodstaVuZel3W+P73TfSHX+U4vPJJkxkOcuCkJ\
fBx9vsSdNKE8ThmLcBxzynA4Z5p7Cw16+S3liA2YVgTHSjZVlk8Q8CzoLRb2ny46JHXvKUts1DOF\
gA7CY+ld4ExjPesbbKcuDr0Z7wD/4RgBakGtjczJM9Ycrf2WGy+SyOGMRonjkoAkT53ENg0EAhI+\
nPORx64fKxzL1Uue1WrSg39FA2GbXOCfE/GvBtptmn5kfhL4O5zkvc0=
exehash=d684304d76b24a6ec6db08b24dd7b53072003db520bc04df38417c905cd9e6d7

523.xalancbmk_r=base=mytest-m64:
# Last updated 2018-03-16 09:32:24
opthash=9c83a7e4cad121e854dd19c1c206a9f3be82becec7584a921970d48295f67260
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNrtk11vmzAUhu/5FRa3FaHrukmLmkoOeKk7G1vYTLQ3FiU0YgNcYajaf18HREa6Sa0m7W5IiPPx\
2uDzPkS68ersZ3FfVgXQD12pG7N0TNeWeafavtmWrXos2vL+eeV+cB0bGitZuaeLL6efXMcJGOVL\
4Pq9af27svF3Jyfg1eXVn8+BlwNPTy9YaOCFgqPAPqIQrZONVYWQcxUxJa9iBENhCykkMFI4oogq\
KjaKMBiiGHh4Ye+nos0Lk/+K/K2ujzO/rB+qWclkT7Os78rKp2ZHdLa1p/JxQ4tat8+vJbLNGpPr\
UZPr5nEvyKqsyf2yyat+W9hP5TG7ViklHMZi/4mHQiIxmdKxKaZUwPQ8mCVnUxwyOoXfIcEhlGxY\
ZbdTiUDTRJAQcIOmoQzzVDCRTImE89h2FeMoohx4O+Cxj4MVo4rgKEnBvMCtReDC6N4e/NJ1giUI\
0nT1pq2Tkq2vGZcr98hl17FkBDz5SuBG2N6x40N3jaLgStkNDpr/EPwbCFyHLYH1CFN8i+ygByQG\
DziLJVxjguXNZNIIyNBFqYyh+qOG7/23ym/v+f/H6ADi7yRe6LsfRd6Zy6OlB5gAGFgj4Xug/Iuj\
DnuzZM/wDOAXPWJ6Gw==
exehash=575e389a87b341a471c8a731bc5d2e21a5d12ead85d5bbdab9aea9ebe2c101be

531.deepsjeng_r=base=mytest-m64:
# Last updated 2018-03-16 09:32:27
opthash=9ae9c931ac302d90e14746a840d5d88b74f0195ee4f231b68e3a7009bb4536ef
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNqNUF1LwzAUfc+vuOR1dJ04BMs66Eec1aQJ/YDNl+BqN6qukX4I/nuzzjorgs1L7uWee849J1Sl\
cXh8yXfFaw7qrSlUWVuobqoia2TVlk9FJd/zqth92PgCI13WGmLj2fR6NscIeZwJC7DZ1pW5LUpz\
P5nAr2ccruZgZGCoXmCqwPBjQTz9hT5x09WxZw6lkhHGo83XWDppwmWcChGROJZckJAJTbgHg1+e\
+U9YKrQMLGrVVlm+xMizwFuv7X9P65HcveMisfHgUoy0O0+kN9RZxXo2vLqbuiT0buUZMNIGRtwC\
LRiw4IHoxc5TR0jWSeRIwaPEcQMaJJtet7OIEQ3C+zGRn6phVrBQ2+c8a+rlAP7tF6CLg/pjcvvT\
QLfN02OQP1L8BFejnsQ=
exehash=34b2d16d90447c95eed4d38c170fa7f86fc0a82ec7b7b14dce5c1d7f9c3c870d

541.leela_r=base=mytest-m64:
# Last updated 2018-03-16 09:32:32
opthash=9a68e8b5b357374dc236dd772b7c5f5a508f3b2f4a356efb88ab8496ba08f8fb
baggage=
compiler_version=\
@eNptkk1v2zAMhu/5FTq2aGTna1lgwKdg2HZZhy4dejNkmZHpypKnj8Trr59kw3Vb7GBZfF6KokiS\
49PTsfj95eHX9/sfxf3PU/hli0eLSpDSo3QUFbEdcJssTswIcBnpD/tiv6MGqpo5KlH5fnHU6ozC\
B0au6OqMJEnKJ0Yo7Qycsc9Tb02wWqYqNIOV2poZSAMJHNVZfxAiCkoMSksfosm8dq7L0jRYLygl\
S8ZEEq7bVxZOgGKlBFpq7awzrJvRELiabVcbYJXNO22xnzGvgT+HOuQGJDALUxb2r3XQ0heJ5exc\
FLxnBXPQowu0Qjvg4OPVFVVFoefQOdTKzoeE8tQr/OOB6rIB7mZJMiU8E2BzvuR3d8ug87jQaDTs\
wpZnbcKz1JJVb14bFcquLhfu+U0albU4pT+41LqFscohw7S5tOmA18mnZEUFb8ZdskqboXtTUlhG\
rfXS4RklfLi3Zahc+MDQVlev9YJwpGHvmhrdU+ASOwtRT4L+vmpjwHjRWOYhUtfJacul1mIynFeQ\
CwgXI58YM7wutpsc94d9YHGWq/x/k3sauk9ixjIjwwwsBOfkAsaGdpFdsks+k81qvVlt11ty8xBG\
5xtzI6frwy25+Xo83pLFPxPUIO4=
compile_options=\
@eNqNUFtLwzAUfs+vOOR1pJ14Acs66CXOaNuEXqD6ElztRtU10ovgvzd2Vq2ILi855Hz5bpGqye7u\
sdxUTyWo565SdWuhtmuqopNNX99XjXwpm2rzauMjjPTYaoiN58b5/BQj5PFQWIDNvm3MdVWb29kM\
fhyyOzsBUgBRo4ChgPiJoJ6+Ip+62QoIMz7epJOlXCaZEDFNEskFjUKhWbZA+PEX6R4bCM0Ni1b1\
TVEuMfIs8PLc/tfPiOTuFRepjSf2MNKRPJFdBM4q0bup1WHr0si7lCPgb+8YcQu0CgvZLdXoIcjA\
QvM0dqTgceq4LGDpzSg25MIoYNH1IeXup2lBsFDrh7Lo2uUE/hkSYOgg8A8p69cAw2+evbf3rbo3\
tWGXyQ==
exehash=1fcfa394e3d38d8d398a83e594c743a4c9e7a2cce7c3547d9bf3522df7c107ad

