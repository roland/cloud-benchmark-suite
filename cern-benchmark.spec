Name: cern-benchmark
Version: 1.0
Release: 0
Summary: CERN Benchmark Suite
Requires: wget, redhat-lsb-core, gcc, cvmfs, make
Group: Application/System
License: GPL

BuildRoot: %{_tmppath}/%{name}-buildroot
BuildArch: noarch
Prefix: %{_prefix}
Vendor: Cristovao Cordeiro <cristovao.cordeiro@cern.ch>
Source0: %{name}-%{version}.tar.gz

%define _binaries_in_noarch_packages_terminate_build   0

%description
The CERN Benchmark suite is a tool which aggregates several different
benchmarks in one single application. It is built to comply with several
different use cases by allowing the users to specify which benchmarks to run.

%prep
%setup -q

%build

%install
make prepare ROOTDIR="$RPM_BUILD_ROOT"/

%post
cd /opt/cern-benchmark
make rpminstall
test -e /usr/bin/cern-benchmark || ln -s /opt/cern-benchmark/cern-benchmark /usr/bin/cern-benchmark

%clean
rm -rf $RPM_BUILD_ROOT

%files
/opt/cern-benchmark/*

%changelog
* Fri Apr 8 2016 Cristóvão Cordeiro <cristovao.cordeiro@cern.ch>
- First release. Default BMK suite pkg plus docker sub package
