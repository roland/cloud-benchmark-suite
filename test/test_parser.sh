#!/bin/bash

set -x
cd `dirname $0`
TESTDIR=`pwd`

export BENCHMARK_TARGET=machine
export init_tests=1506953393
export init_kv_test=1506953393
export end_kv_test=1506953398
export end_tests=1506954182

export DB12=18.7500457765
export HWINFO=i6_2_f6m61s2_mhz2194.916
export FREE_TEXT=""
export PNODE=
export MP_NUM=2

export HYPER_BENCHMARK=1
export HYPER_1minLoad_1=1.1
export HYPER_1minLoad_2=1.1
export HYPER_1minLoad_3=1.1

python $TESTDIR/../run/parser.py -i f2429109a5a6_a1842eb2-06a5-4ba9-87c8-86ca0f2c49b3 -c "suite-CI" -v  -f data/result_profile.json -p 172.17.0.2 -d data/bmk_run -v foo

echo -e "\nDUMP JSON"
cat data/result_profile.json
echo -e "\nTEST DONE" 

cd $TESTDIR/..
python -c "import run.parser; run.parser.print_results_from_file(\"test/data/result_profile.json\")"
