# Invocation command line:
# /spec2017/bin/harness/runcpu --configfile cern-gcc-linux-x86.cfg --nobuild --noreportable --iterations 1 --noreportable --nopower --runmode rate --tune base --size refrate 508.namd_r 510.parest_r 511.povray_r 526.blender_r
# output_root was not used for this run
############################################################################
#------------------------------------------------------------------------------
# SPEC CPU2017 config file for: gcc / g++ / gfortran on Linux x86
#------------------------------------------------------------------------------
#
# Usage: (1) Copy this to a new name
#             cd $SPEC/config
#             cp Example-x.cfg myname.cfg
#        (2) Change items that are marked 'EDIT' (search for it)
#
# SPEC tested this config file with:
#    Compiler version(s):    4.4.7, 4.9.2, 5.2.0, 6.3.0
#    Operating system(s):    Oracle Linux Server 6.5 and 7.2 /
#                            Red Hat Enterprise Linux Server 6.5 and 7.2
#    Hardware:               Xeon
#
# If your system differs, this config file might not work.
# You might find a better config file at http://www.spec.org/cpu2017/results
#
# Known Limitations with GCC 4
#
#   (1) Possible problem: compile time messages
#                             error: unrecognized command line option '...'
#      Recommendation:    Use a newer version of the compiler.
#                         If that is not possible, remove the unrecognized
#                         option from this config file.
#
#   (2) Possible problem: run time errors messages
#           527.cam4_r or 627.cam4_s    *** Miscompare of cam4_validate.txt
#      Recommendation: Use a newer version of the compiler.
#                      If that is not possible, try reducing the optimization.
#
# Compiler issues: Contact your compiler vendor, not SPEC.
# For SPEC help:   http://www.spec.org/cpu2017/Docs/techsupport.html
#------------------------------------------------------------------------------


#--------- Label --------------------------------------------------------------
# Arbitrary string to tag binaries
#                  Two Suggestions: # (1) EDIT this label as you try new ideas.
%define label mytest                # (2)      Use a label meaningful to *you*.


#--------- Preprocessor -------------------------------------------------------
%ifndef %{bits}                # EDIT to control 32 or 64 bit compilation.  Or,
%   define  bits        64     #      you can set it on the command line using:
%endif                         #      'runcpu --define bits=nn'

%ifndef %{build_ncpus}         # EDIT to adjust number of simultaneous compiles.
%   define  build_ncpus 8      #      Or, you can set it on the command line:
%endif                         #      'runcpu --define build_ncpus=nn'

# Don't change this part.
%define    os           LINUX
%if %{bits} == 64
%   define model        -m64
%elif %{bits} == 32
%   define model        -m32
%else
%   error Please define number of bits - see instructions in config file
%endif


#--------- Global Settings ----------------------------------------------------
# For info, see:
#            https://www.spec.org/cpu2017/Docs/config.html#fieldname
#   Example: https://www.spec.org/cpu2017/Docs/config.html#tune

command_add_redirect = 1
flagsurl000          = /spec2017/config/flags/gcc.xml
ignore_errors        = 1
iterations           = 1
label                = %{label}-m%{bits}
line_width           = 1020
log_line_width       = 1020
makeflags            = --jobs=%{build_ncpus}
mean_anyway          = 1
output_format        = txt,html,cfg,pdf,csv
preenv               = 1
reportable           = 0
tune                 = base


#--------- How Many CPUs? -----------------------------------------------------
# Both SPECrate and SPECspeed can test multiple chips / cores / hw threads
#    - For SPECrate,  you set the number of copies.
#    - For SPECspeed, you set the number of threads.
# See: https://www.spec.org/cpu2017/Docs/system-requirements.html#MultipleCPUs
#
#    q. How many should I set?
#    a. Unknown, you will have to try it and see!
#
# To get you started, some suggestions:
#
#     copies - This config file defaults to testing only 1 copy.   You might
#              try changing it to match the number of cores on your system,
#              or perhaps the number of virtual CPUs as reported by:
#                     grep -c processor /proc/cpuinfo
#              Be sure you have enough memory.  See:
#              https://www.spec.org/cpu2017/Docs/system-requirements.html#memory
#
#     threads - This config file sets a starting point.  You could try raising
#               it.  A higher thread count is much more likely to be useful for
#               fpspeed than for intspeed.
#
intrate,fprate:
   copies           = 1   # EDIT to change number of copies (see above)
intspeed,fpspeed:
   threads          = 4   # EDIT to change number of OpenMP threads (see above)


#------- Compilers ------------------------------------------------------------
default:
#                    EDIT: the directory where your compiler is installed
%ifndef %{gcc_dir}
%   define  gcc_dir      /usr # /opt/rh/devtoolset-2/root/usr/  #/SW/compilers/GCC/Linux/x86_64/gcc-6.3.0
%endif

# EDIT if needed: the preENV line adds library directories to the runtime
#      path.  You can adjust it, or add lines for other environment variables.
#      See: https://www.spec.org/cpu2017/Docs/config.html#preenv
#      and: https://gcc.gnu.org/onlinedocs/gcc/Environment-Variables.html
   preENV_LD_LIBRARY_PATH  = %{gcc_dir}/lib64/:%{gcc_dir}/lib/:/lib64
  #preENV_LD_LIBRARY_PATH  = %{gcc_dir}/lib64/:%{gcc_dir}/lib/:/lib64:%{ENV_LD_LIBRARY_PATH}
   SPECLANG                = %{gcc_dir}/bin/
   CC                      = $(SPECLANG)gcc     -std=c99   %{model}
   CXX                     = $(SPECLANG)g++                %{model}
   FC                      = $(SPECLANG)gfortran           %{model}
   # How to say "Show me your version, please"
   CC_VERSION_OPTION       = -v
   CXX_VERSION_OPTION      = -v
   FC_VERSION_OPTION       = -v

default:
%if %{bits} == 64
   sw_base_ptrsize = 64-bit
   sw_peak_ptrsize = Not Applicable
%else
   sw_base_ptrsize = 32-bit
   sw_peak_ptrsize = 32-bit
%endif


#--------- Portability --------------------------------------------------------
default:               # data model applies to all benchmarks
%if %{bits} == 32
    # Strongly recommended because at run-time, operations using modern file
    # systems may fail spectacularly and frequently (or, worse, quietly and
    # randomly) if a program does not accommodate 64-bit metadata.
    EXTRA_PORTABILITY = -D_FILE_OFFSET_BITS=64
%else
    EXTRA_PORTABILITY = -DSPEC_LP64
%endif

# Benchmark-specific portability (ordered by last 2 digits of bmark number)

500.perlbench_r,600.perlbench_s:  #lang='C'
%if %{bits} == 32
%   define suffix IA32
%else
%   define suffix X64
%endif
   PORTABILITY    = -DSPEC_%{os}_%{suffix}

521.wrf_r,621.wrf_s:  #lang='F,C'
   CPORTABILITY  = -DSPEC_CASE_FLAG
   FPORTABILITY  = -fconvert=big-endian

523.xalancbmk_r,623.xalancbmk_s:  #lang='CXX'
   PORTABILITY   = -DSPEC_%{os}

526.blender_r:  #lang='CXX,C'
   PORTABILITY   = -funsigned-char -DSPEC_LINUX

527.cam4_r,627.cam4_s:  #lang='F,C'
   PORTABILITY   = -DSPEC_CASE_FLAG

628.pop2_s:  #lang='F,C'
   CPORTABILITY    = -DSPEC_CASE_FLAG
   FPORTABILITY    = -fconvert=big-endian


#-------- Tuning Flags common to Base and Peak --------------------------------

#
# Speed (OpenMP and Autopar allowed)
#
%if %{bits} == 32
   intspeed,fpspeed:
   #
   # Many of the speed benchmarks (6nn.benchmark_s) do not fit in 32 bits
   # If you wish to run SPECint2017_speed or SPECfp2017_speed, please use
   #
   #     runcpu --define bits=64
   #
   fail_build = 1
%else
   intspeed,fpspeed:
      EXTRA_OPTIMIZE = -fopenmp -DSPEC_OPENMP
   fpspeed:
      #
      # 627.cam4 needs a big stack; the preENV will apply it to all
      # benchmarks in the set, as required by the rules.
      #
      preENV_OMP_STACKSIZE = 120M
%endif


#--------  Baseline Tuning Flags ----------------------------------------------
#
# EDIT if needed -- Older GCC might not support some of the optimization
#                   switches here.  See also 'About the -fno switches' below.
#
default=base:         # flags for all base
   OPTIMIZE       = -g -O3 #-march=native -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize

intrate,intspeed=base: # flags for integer base
%if %{bits} == 32
    EXTRA_COPTIMIZE = -fgnu89-inline -fno-strict-aliasing
%else
    EXTRA_COPTIMIZE = -fno-strict-aliasing
    LDCFLAGS        = -z muldefs
%endif
# Notes about the above
#  - 500.perlbench_r/600.perlbench_s needs -fno-strict-aliasing.
#  - 502.gcc_r/602.gcc_s             needs -fgnu89-inline or -z muldefs
#  - For 'base', all benchmarks in a set must use the same options.
#  - Therefore, all base benchmarks get the above.  See:
#       www.spec.org/cpu2017/Docs/runrules.html#BaseFlags
#       www.spec.org/cpu2017/Docs/benchmarks/500.perlbench_r.html
#       www.spec.org/cpu2017/Docs/benchmarks/502.gcc_r.html


#--------  Peak Tuning Flags ----------------------------------------------
default=peak:
   basepeak = yes  # if you develop some peak tuning, remove this line.
   #
   # -----------------------
   # About the -fno switches
   # -----------------------
   #
   # For 'base', this config file (conservatively) disables some optimizations.
   # You might want to try turning some of them back on, by creating a 'peak'
   # section here, with individualized benchmark options:
   #
   #        500.perlbench_r=peak:
   #           OPTIMIZE = this
   #        502.gcc_r=peak:
   #           OPTIMIZE = that
   #        503.bwaves_r=peak:
   #           OPTIMIZE = other   .....(and so forth)
   #
   # If you try it:
   #   - You must remove the 'basepeak' option, above.
   #   - You will need time and patience, to diagnose and avoid any errors.
   #   - perlbench is unlikely to work with strict aliasing
   #   - Some floating point benchmarks may get wrong answers, depending on:
   #         the particular chip
   #         the version of GCC
   #         other optimizations enabled
   #         -m32 vs. -m64
   #   - See: http://www.spec.org/cpu2017/Docs/config.html
   #   - and: http://www.spec.org/cpu2017/Docs/runrules.html


#------------------------------------------------------------------------------
# Tester and System Descriptions - EDIT all sections below this point
#------------------------------------------------------------------------------
#   For info about any field, see
#             https://www.spec.org/cpu2017/Docs/config.html#fieldname
#   Example:  https://www.spec.org/cpu2017/Docs/config.html#hw_memory
#-------------------------------------------------------------------------------

#--------- EDIT to match your version -----------------------------------------
default:
   sw_compiler000   = C/C++/Fortran: Version 6.2.0 of GCC, the
   sw_compiler001   = GNU Compiler Collection

#--------- EDIT info about you ------------------------------------------------
# To understand the difference between hw_vendor/sponsor/tester, see:
#     https://www.spec.org/cpu2017/Docs/config.html#test_sponsor
intrate,intspeed,fprate,fpspeed: # Important: keep this line
   hw_vendor          = My Corporation
   tester             = My Corporation
   test_sponsor       = My Corporation
   license_num        = nnn (Your SPEC license number)
#  prepared_by        = # Ima Pseudonym                       # Whatever you like: is never output


#--------- EDIT system availability dates -------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
   hw_avail           = # Nov-2099                            # Date of LAST hardware component to ship
   sw_avail           = # Nov-2099                            # Date of LAST software component to ship

#--------- EDIT system information --------------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
#  hw_cpu_name        = # Intel Xeon E9-9999 v9               # chip name
   hw_cpu_nominal_mhz = # 9999                                # Nominal chip frequency, in MHz
   hw_cpu_max_mhz     = # 9999                                # Max chip frequency, in MHz
#  hw_disk            = # 9 x 9 TB SATA III 9999 RPM          # Size, type, other perf-relevant info
   hw_model           = # TurboBlaster 3000                   # system model name
#  hw_nchips          = # 99                                  # number chips enabled
   hw_ncores          = # 9999                                # number cores enabled
   hw_ncpuorder       = # 1-9 chips                           # Ordering options
   hw_nthreadspercore = # 9                                   # number threads enabled per core
   hw_other           = # TurboNUMA Router 10 Gb              # Other perf-relevant hw, or "None"

#  hw_memory001       = # 999 GB (99 x 9 GB 2Rx4 PC4-2133P-R, # The 'PCn-etc' is from the JEDEC
#  hw_memory002       = # running at 1600 MHz)                # label on the DIMM.

   hw_pcache          = # 99 KB I + 99 KB D on chip per core  # Primary cache size, type, location
   hw_scache          = # 99 KB I+D on chip per 9 cores       # Second cache or "None"
   hw_tcache          = # 9 MB I+D on chip per chip           # Third  cache or "None"
   hw_ocache          = # 9 GB I+D off chip per system board  # Other cache or "None"

   fw_bios            = # American Megatrends 39030100 02/29/2016 # Firmware information
#  sw_file            = # ext99                               # File system
#  sw_os001           = # Linux Sailboat                      # Operating system
#  sw_os002           = # Distribution 7.2 SP1                # and version
   sw_other           = # TurboHeap Library V8.1              # Other perf-relevant sw, or "None"
#  sw_state           = # Run level 99                        # Software state.

# Note: Some commented-out fields above are automatically set to preliminary
# values by sysinfo
#       https://www.spec.org/cpu2017/Docs/config.html#sysinfo
# Uncomment lines for which you already know a better answer than sysinfo


# The following settings were obtained by running the sysinfo_program
# 'specperl $[top]/bin/sysinfo' (sysinfo:SHA:ecd2bef08f316af97f5a7768b641e2a3307c1b4b68efb5a57fa76367d790d233)
default:
notes_plat_sysinfo_000 = Sysinfo program /spec2017/bin/sysinfo
notes_plat_sysinfo_005 = Rev: r5797 of 2017-06-14 96c45e4568ad54c135fd618bcc091c0f
notes_plat_sysinfo_010 = running on 4bdabd3c3191 Wed Dec 20 15:54:25 2017
notes_plat_sysinfo_015 =
notes_plat_sysinfo_020 = SUT (System Under Test) info as seen by some common utilities.
notes_plat_sysinfo_025 = For more information on this section, see
notes_plat_sysinfo_030 =    https://www.spec.org/cpu2017/Docs/config.html#sysinfo
notes_plat_sysinfo_035 =
notes_plat_sysinfo_040 = From /proc/cpuinfo
notes_plat_sysinfo_045 =    model name : Intel Core Processor (Broadwell)
notes_plat_sysinfo_050 =       2  "physical id"s (chips)
notes_plat_sysinfo_055 =       2 "processors"
notes_plat_sysinfo_060 =    cores, siblings (Caution: counting these is hw and system dependent. The following
notes_plat_sysinfo_065 =    excerpts from /proc/cpuinfo might not be reliable.  Use with caution.)
notes_plat_sysinfo_070 =       cpu cores : 1
notes_plat_sysinfo_075 =       siblings  : 1
notes_plat_sysinfo_080 =       physical 0: cores 0
notes_plat_sysinfo_085 =       physical 1: cores 0
notes_plat_sysinfo_090 =
notes_plat_sysinfo_095 = From lscpu:
notes_plat_sysinfo_100 =      Architecture:          x86_64
notes_plat_sysinfo_105 =      CPU op-mode(s):        32-bit, 64-bit
notes_plat_sysinfo_110 =      Byte Order:            Little Endian
notes_plat_sysinfo_115 =      CPU(s):                2
notes_plat_sysinfo_120 =      On-line CPU(s) list:   0,1
notes_plat_sysinfo_125 =      Thread(s) per core:    1
notes_plat_sysinfo_130 =      Core(s) per socket:    1
notes_plat_sysinfo_135 =      Socket(s):             2
notes_plat_sysinfo_140 =      NUMA node(s):          1
notes_plat_sysinfo_145 =      Vendor ID:             GenuineIntel
notes_plat_sysinfo_150 =      CPU family:            6
notes_plat_sysinfo_155 =      Model:                 61
notes_plat_sysinfo_160 =      Model name:            Intel Core Processor (Broadwell)
notes_plat_sysinfo_165 =      Stepping:              2
notes_plat_sysinfo_170 =      CPU MHz:               2194.916
notes_plat_sysinfo_175 =      BogoMIPS:              4389.83
notes_plat_sysinfo_180 =      Virtualization:        VT-x
notes_plat_sysinfo_185 =      Hypervisor vendor:     KVM
notes_plat_sysinfo_190 =      Virtualization type:   full
notes_plat_sysinfo_195 =      L1d cache:             32K
notes_plat_sysinfo_200 =      L1i cache:             32K
notes_plat_sysinfo_205 =      L2 cache:              4096K
notes_plat_sysinfo_210 =      NUMA node0 CPU(s):     0,1
notes_plat_sysinfo_215 =
notes_plat_sysinfo_220 = /proc/cpuinfo cache data
notes_plat_sysinfo_225 =    cache size : 4096 KB
notes_plat_sysinfo_230 =
notes_plat_sysinfo_235 = From numactl --hardware  WARNING: a numactl 'node' might or might not correspond to a
notes_plat_sysinfo_240 = physical chip.
notes_plat_sysinfo_245 =
notes_plat_sysinfo_250 = From /proc/meminfo
notes_plat_sysinfo_255 =    MemTotal:        3531712 kB
notes_plat_sysinfo_260 =    HugePages_Total:       0
notes_plat_sysinfo_265 =    Hugepagesize:       2048 kB
notes_plat_sysinfo_270 =
notes_plat_sysinfo_275 = /usr/bin/lsb_release -d
notes_plat_sysinfo_280 =    Scientific Linux CERN SLC release 6.9 (Carbon)
notes_plat_sysinfo_285 =
notes_plat_sysinfo_290 = From /etc/*release* /etc/*version*
notes_plat_sysinfo_295 =    redhat-release: Scientific Linux CERN SLC release 6.9 (Carbon)
notes_plat_sysinfo_300 =    system-release: Scientific Linux CERN SLC release 6.9 (Carbon)
notes_plat_sysinfo_305 =    system-release-cpe: cpe:/o:redhat:enterprise_linux:6.9:ga
notes_plat_sysinfo_310 =
notes_plat_sysinfo_315 = uname -a:
notes_plat_sysinfo_320 =    Linux 4bdabd3c3191 3.10.0-514.10.2.el7.x86_64 #1 SMP Fri Mar 3 00:04:05 UTC 2017
notes_plat_sysinfo_325 =    x86_64 x86_64 x86_64 GNU/Linux
notes_plat_sysinfo_330 =
notes_plat_sysinfo_335 =
notes_plat_sysinfo_340 = SPEC is set to: /spec2017
notes_plat_sysinfo_345 =    Filesystem     Type  Size  Used Avail Use% Mounted on
notes_plat_sysinfo_350 =    /dev/vdb       ext4   30G   28G  463M  99% /spec2017
notes_plat_sysinfo_355 =
notes_plat_sysinfo_360 = (End of data from sysinfo program)
hw_cpu_name    = Intel Core (Broadwell)
hw_disk        = 30 GB  add more disk info here
hw_memory000   = 3.368 GB fixme: If using DDR3, format is:
hw_memory001   = 'N GB (M x N GB nRxn PCn-nnnnnR-n, ECC)'
hw_nchips      = 2
prepared_by    = (is never output, only tags rawfile)
sw_file        = ext4
sw_os000       = Scientific Linux CERN SLC release 6.9 (Carbon)
sw_os001       = 3.10.0-514.10.2.el7.x86_64
sw_state       = Run level N (add definition here)
# End of settings added by sysinfo_program


# The following section was added automatically, and contains settings that
# did not appear in the original configuration file, but were added to the
# raw file after the run.
default:
notes_000 =Environment variables set by runcpu before the start of the run:
notes_005 =LD_LIBRARY_PATH = "/usr/lib64/:/usr/lib/:/lib64"
notes_010 =
